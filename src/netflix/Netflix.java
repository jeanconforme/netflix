/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package netflix;

/**
 *
 * @author USUARIO
 */
public class Netflix {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Pelicula AvengerEG = new Pelicula();
        AvengerEG.setNombre("Avengers EndGame");
        AvengerEG.setAnioEstreno(2019);
        AvengerEG.setDirector("Hermanos Russó");
        AvengerEG.setPrecio(20.50);
        AvengerEG.setCodigo(01);
        
        Pelicula AvengerIW = new Pelicula();
        AvengerIW.setNombre("Avengers Infinity War");
        AvengerIW.setAnioEstreno(2018);
        AvengerIW.setDirector("Hermanos Russó");
        AvengerIW.setPrecio(17.75);
        AvengerIW.setCodigo(02);

        Favorita favorita1 = new Favorita();
        favorita1.setPelicula(AvengerEG);
        favorita1.setComentario("Esta buena la pelicula");
        
        Favorita favorita2 = new Favorita();
        favorita2.setPelicula(AvengerIW);
        favorita2.setComentario("Final inesperado");
       
        ListaFavorita listafavorita = new ListaFavorita();
        listafavorita.AgregarPeliculaFavorita(favorita1);
        listafavorita.AgregarPeliculaFavorita(favorita2);
        
        listafavorita.QuitarPeliculaDeFavorita();
        listafavorita.ImprimirFavoritas();
    }}