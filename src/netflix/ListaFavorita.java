/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package netflix;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author USUARIO
 */
public class ListaFavorita implements ImprimirListaFavorita{
    
    List<Favorita> ListaPeliculaFavorita;
    
    public ListaFavorita(){
        this.ListaPeliculaFavorita = new ArrayList<>();
    }
    
    public void AgregarPeliculaFavorita (Favorita pelicula){
        this.ListaPeliculaFavorita.add(pelicula);
    }
    public void QuitarPeliculaDeFavorita(){
        this.ListaPeliculaFavorita.remove(1);
        
    }
    @Override
    public void ImprimirFavoritas() {
        for (Favorita PeliculaFavorita : ListaPeliculaFavorita){
            System.out.println(PeliculaFavorita.getComentario());
            System.out.println(PeliculaFavorita.getPelicula().getNombre());
    }
}}